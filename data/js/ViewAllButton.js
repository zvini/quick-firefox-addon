function ViewAllButton (base, port) {

    var icon = Icon('externalLink')

    var element = document.createElement('div')
    element.className = 'BottomButton ViewAllButton'
    element.title = 'View All on Zvini'
    element.appendChild(icon.element)
    LinkClick(element, port, base + 'bookmarks/')
    return element
}
