var Request = require('sdk/request').Request

exports.BookmarkAddRequest = function (base, url, title, callback) {
    var request = Request({
        url: base + 'api-call/bookmark/add',
        content: {
            session_auth: 1,
            url: url,
            title: title,
        },
        onComplete: callback,
    })
    request.get()
}
