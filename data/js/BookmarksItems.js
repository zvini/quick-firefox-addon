function BookmarksItems (port, bookmarks) {

    function applyFilter () {

        if (selectedItem !== null) {
            selectedItem.deselect()
            selectedItem = null
        }

        if (emptyVisible) {
            emptyVisible = false
            contentElement.removeChild(emptyElement)
        }

        var keyword = searchInput.value
        if (keyword === '') {
            visibleItems = items
            items.forEach(function (item) {
                item.reset()
            })
        } else {

            var pattern = keyword.replace(/[[\](){}/|.*+?^$]/gi, function (ch) {
                return '\\' + ch
            })

            visibleItems = []

            var matched = false
            items.forEach(function (item) {
                if (item.applyFilter(pattern)) {
                    matched = true
                    visibleItems.push(item)
                }
            })

            if (!matched && !emptyVisible) {
                emptyVisible = true
                contentElement.appendChild(emptyElement)
            }

            if (matched > 0) select(visibleItems[0])

        }

    }

    function fixScroll () {

        var itemRect = selectedItem.element.getBoundingClientRect()
        var wrapperRect = element.getBoundingClientRect()

        var overflow = itemRect.bottom - wrapperRect.bottom
        if (overflow > 0) element.scrollTop += overflow

        var underflow = wrapperRect.top + searchElement.offsetHeight - itemRect.top
        if (underflow > 0) element.scrollTop -= underflow

    }

    function select (item) {
        if (selectedItem !== null) selectedItem.deselect()
        item.select()
        selectedItem = item
    }

    var classPrefix = 'BookmarksItems'

    var items = []

    var searchInput = document.createElement('input')
    searchInput.placeholder = 'Search...'
    searchInput.className = classPrefix + '-searchInput'
    searchInput.addEventListener('keydown', function (e) {

        if (e.altKey || e.metaKey || e.shiftKey) return

        var keyCode = e.keyCode
        if (e.ctrlKey) {
            if (keyCode === 13) {
                if (selectedItem !== null) selectedItem.openInNewTab()
            }
        } else {
            if (keyCode === 13) {
                if (selectedItem !== null) selectedItem.open()
            } else if (keyCode === 38) {
                e.preventDefault()
                if (selectedItem === null) {
                    select(visibleItems[visibleItems.length - 1])
                    fixScroll()
                } else {
                    var index = visibleItems.indexOf(selectedItem)
                    if (index > 0) {
                        select(visibleItems[index - 1])
                        fixScroll()
                    }
                }
            } else if (keyCode === 40) {
                e.preventDefault()
                if (selectedItem === null) {
                    select(visibleItems[0])
                    fixScroll()
                } else {
                    var index = visibleItems.indexOf(selectedItem)
                    if (index < visibleItems.length - 1) {
                        select(visibleItems[index + 1])
                        fixScroll()
                    }
                }
            }
        }

    }   )
    searchInput.addEventListener('input', applyFilter)

    var emptyVisible = false

    var emptyElement = document.createElement('div')
    emptyElement.className = classPrefix + '-empty'
    emptyElement.appendChild(document.createTextNode('Nothing found'))

    var searchElement = document.createElement('div')
    searchElement.className = classPrefix + '-search'
    searchElement.appendChild(searchInput)

    var contentElement = document.createElement('div')
    contentElement.className = classPrefix + '-content'
    bookmarks.forEach(function (bookmark) {
        var item = BookmarksItem(port, bookmark, function () {
            select(item)
        }, function () {
            if (selectedItem === item) selectedItem = null
            item.deselect()
        })
        contentElement.appendChild(item.element)
        items.push(item)
    })

    var element = document.createElement('div')
    element.className = classPrefix
    element.appendChild(searchElement)
    element.appendChild(contentElement)

    var selectedItem = null
    var visibleItems = items

    return {
        element: element,
        focus: function () {
            searchInput.focus()
        },
        getState: function () {
            return {
                keyword: searchInput.value,
                scrollTop: element.scrollTop,
                selectionEnd: searchInput.selectionEnd,
                selectionStart: searchInput.selectionStart,
            }
        },
        setState: function (state) {
            searchInput.value = state.keyword
            searchInput.selectionEnd = state.selectionEnd
            searchInput.selectionStart = state.selectionStart
            element.scrollTop = state.scrollTop
            if (searchInput.value !== '') applyFilter()
        },
    }

}
