function ProgressPanel () {

    var element = document.createElement('div')
    element.className = 'ProgressPanel'

    var style = element.style
    style.backgroundImage = 'url(images/progress.svg)'

    var classList = element.classList

    var x = 0
    var interval

    return {
        element: element,
        hide: function () {
            classList.remove('visible')
            clearInterval(interval)
        },
        show: function () {
            classList.add('visible')
            interval = setInterval(function () {
                var value = 'calc(50% + ' + x + 'px) 0'
                style.backgroundPosition = value
                x = (x + 1) % 8
            }, 50)
        },
    }

}
