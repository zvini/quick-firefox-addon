function SignOutButton (listener) {

    var icon = Icon('turnOff')

    var element = document.createElement('div')
    element.className = 'BottomButton SignOutButton'
    element.title = 'Sign Out'
    element.appendChild(icon.element)
    element.addEventListener('click', listener)
    return element

}
