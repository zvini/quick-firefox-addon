function loadBookmarks () {
    if (cachedBookmarks) {
        port.emit('bookmarks', cachedBookmarks)
    } else {
        port.emit('progressStart')
        if (request) request.abort()
        request = BookmarkListRequest(base, function (response) {

            request = null
            port.emit('progressEnd')

            var json = response.json,
                status = response.status

            if (status == 200) {
                port.emit('bookmarks', json)
                cachedBookmarks = json
                setTimeout(function () {
                    cachedBookmarks = null
                }, 60 * 1000)
            } else if (status == 403) {
                if (json == 'NOT_SIGNED_IN') port.emit('notSignedIn')
                else port.emit('unknownError')
            } else {
                port.emit('unknownError')
            }

        })
    }
}

var BookmarkAddRequest = require('./BookmarkAddRequest').BookmarkAddRequest,
    BookmarkListRequest = require('./BookmarkListRequest').BookmarkListRequest,
    Panel = require('sdk/panel').Panel,
    Request = require('sdk/request').Request,
    SessionInvalidateRequest = require('./SessionInvalidateRequest').SessionInvalidateRequest,
    ToggleButton = require('sdk/ui/button/toggle').ToggleButton,
    simplePrefs = require('sdk/simple-prefs'),
    prefs = simplePrefs.prefs,
    self = require('sdk/self'),
    tabs = require('sdk/tabs'),
    setTimeout = require('sdk/timers').setTimeout

var base = prefs.base
simplePrefs.on('base', function () {
    base = prefs.base
    port.emit('base', base)
})

var request = null
var cachedBookmarks = null

var button = ToggleButton({
    id: 'zvini',
    label: 'Zvini',
    icon: {
        16: './icon/16.png',
        32: './icon/32.png',
        64: './icon/64.png',
    },
    onChange: function (state) {
        if (state.checked) {
            panel.show({ position: button })
            panel.on('show', function show () {
                panel.removeListener('show', show)
                loadBookmarks()
            })
        }
    },
})

var panel = Panel({
    width: 299,
    height: 320,
    contentURL: self.data.url('index.html'),
    onHide: function () {
        if (request) {
            request.abort()
            port.emit('progressEnd')
        }
        button.state('window', { checked: false })
    },
})

var port = panel.port
port.on('load', function () {
    port.emit('init', base)
    port.on('bookmark', function () {
        var activeTab = tabs.activeTab
        BookmarkAddRequest(base, activeTab.url, activeTab.title, function (response) {

            var json = response.json,
                status = response.status

            if (status == 200) {
                cachedBookmarks = null
                loadBookmarks()
            } else if (status == 403) {
                if (json == 'NOT_SIGNED_IN') port.emit('notSignedIn')
                else port.emit('unknownError')
            } else {
                port.emit('unknownError')
            }

        })
    })
    port.on('open', function (url) {
        tabs.activeTab.url = url
        tabs.activeTab.reload()
        panel.hide()
    })
    port.on('openInNewTab', function (url) {
        tabs.open(url)
    })
    port.on('openSettings', function () {

        panel.hide()

        tabs.open({
            url: 'about:addons',
            onReady: function(tab) {
                tab.attach({
                    contentScriptWhen: 'end',
                    contentScript:
                        'AddonManager.getAddonByID("' + self.id + '", function (aAddon) {\n' +
                            'unsafeWindow.gViewController.commands.cmd_showItemDetails.doCommand(aAddon, true)\n' +
                        '})\n',
                })
            },
        })

    })
    port.on('refresh', function () {
        cachedBookmarks = null
        loadBookmarks()
    })
    port.on('retry', function () {
        if (request) {
            request.abort()
            port.emit('progressEnd')
        }
        loadBookmarks()
    })
    port.on('signOut', function () {
        cachedBookmarks = null
        panel.hide()
        SessionInvalidateRequest(base)
    })
})
