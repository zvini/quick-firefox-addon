function BookmarkButton (port) {

    var icon = Icon('bookmark')

    var element = document.createElement('div')
    element.className = 'BottomButton BookmarkButton'
    element.title = 'Bookmark'
    element.appendChild(icon.element)
    element.addEventListener('click', function (e) {
        port.emit('bookmark')
    })
    return element

}
