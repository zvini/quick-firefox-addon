function BookmarksPanel (base, port, bookmarks, signOutListener) {

    var classPrefix = 'BookmarksPanel'

    var bookmarkButton = BookmarkButton(port)

    var viewAllButton = ViewAllButton(base, port)

    var refreshButton = RefreshButton(port)

    var signOutButton = SignOutButton(signOutListener)

    var bookmarksItems = null

    var element = document.createElement('div')
    element.className = classPrefix
    if (bookmarks.length === 0) {

        var emptyElement = document.createElement('div')
        emptyElement.appendChild(document.createTextNode('No bookmarks'))
        emptyElement.className = classPrefix + '-empty'

        element.appendChild(emptyElement)

    } else {
        bookmarksItems = BookmarksItems(port, bookmarks)
        element.appendChild(bookmarksItems.element)
    }
    element.appendChild(bookmarkButton)
    element.appendChild(viewAllButton)
    element.appendChild(refreshButton)
    element.appendChild(signOutButton)

    return {
        element: element,
        isBookmarksPanel: true,
        focus: function () {
            if (bookmarksItems === null) return
            bookmarksItems.focus()
        },
        getState: function () {
            if (bookmarksItems === null) return null
            return bookmarksItems.getState()
        },
        setState: function (state) {
            if (bookmarksItems === null) return
            bookmarksItems.setState(state)
        },
    }

}
