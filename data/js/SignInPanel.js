function SignInPanel (port, base) {

    var classPrefix = 'SignInPanel'

    var info = 'You should sign in to use this addon.'
    var infoNode = document.createTextNode(info)

    var infoElement = document.createElement('div')
    infoElement.className = classPrefix + '-info'
    infoElement.appendChild(infoNode)

    var buttonElement = document.createElement('div')
    buttonElement.className = classPrefix + '-button'
    buttonElement.appendChild(document.createTextNode('Sign In'))
    LinkClick(buttonElement, port, base + 'sign-in/')

    var settingsElement = document.createElement('div')
    settingsElement.className = classPrefix + '-settings'
    settingsElement.addEventListener('click', function () {
        port.emit('openSettings')
    })

    var element = document.createElement('div')
    element.className = classPrefix
    element.appendChild(infoElement)
    element.appendChild(buttonElement)
    element.appendChild(settingsElement)

    return { element: element }

}
