var Request = require('sdk/request').Request

exports.SessionInvalidateRequest = function (base) {
    var request = Request({
        url: base + 'api-call/session/invalidate',
        content: { session_auth: 1 },
    })
    request.get()
}
