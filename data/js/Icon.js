function Icon (name) {
    var element = document.createElement('div')
    element.className = 'Icon ' + name
    return { element: element }
}
