(function () {
    var port = addon.port
    port.emit('load')
    port.on('init', function (base) {

        function createSignInPanel () {
            return SignInPanel(port, base)
        }

        function replacePanel (panel) {
            body.removeChild(contentPanel.element)
            showPanel(panel)
        }

        function showPanel (panel) {
            contentPanel = panel
            body.insertBefore(contentPanel.element, progressPanel.element)
        }

        function showSignInPanel () {
            replacePanel(createSignInPanel())
        }

        var errorPanel = ErrorPanel(port)

        var progressPanel = ProgressPanel()

        var body = document.body
        body.appendChild(progressPanel.element)

        port.on('progressEnd', progressPanel.hide)
        port.on('progressStart', progressPanel.show)
        port.on('notSignedIn', showSignInPanel)
        port.on('base', function (_base) {
            base = _base
        })
        port.on('unknownError', function () {
            replacePanel(errorPanel)
        })
        port.on('bookmarks', function (bookmarks) {
            var state = null
            if (contentPanel.isBookmarksPanel) {
                state = contentPanel.getState()
            }
            var bookmarksPanel = BookmarksPanel(base, port, bookmarks, function () {
                port.emit('signOut')
                showSignInPanel()
            })
            replacePanel(bookmarksPanel)
            if (state !== null) bookmarksPanel.setState(state)
            bookmarksPanel.focus()
        })

        var contentPanel
        showPanel(createSignInPanel())

    })
})()
