function RefreshButton (port) {

    var icon = Icon('refresh')

    var element = document.createElement('div')
    element.className = 'BottomButton RefreshButton'
    element.title = 'Refresh'
    element.appendChild(icon.element)
    element.addEventListener('click', function (e) {
        port.emit('refresh')
    })
    return element

}
