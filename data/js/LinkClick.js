function LinkClick (element, port, url) {

    function open () {
        port.emit('open', url)
    }

    function openInNewTab () {
        port.emit('openInNewTab', url)
    }

    element.addEventListener('click', function (e) {
        if (!e.altKey && !e.metaKey && !e.shiftKey) {
            if (e.ctrlKey) openInNewTab()
            else open()
        }
    })

    var down = false
    addEventListener('mousedown', function (e) {
        if (e.button == 1 && e.target == element) down = true
    })
    addEventListener('mouseup', function (e) {
        if (e.button == 1) {
            down = false
            if (e.target == element) openInNewTab()
        }
    })

    return {
        open: open,
        openInNewTab: openInNewTab,
    }

}
