var Request = require('sdk/request').Request

exports.BookmarkListRequest = function (base, callback) {

    var aborted = false

    var request = Request({
        url: base + 'api-call/bookmark/list',
        content: { session_auth: 1 },
        onComplete: function (response) {
            if (!aborted) callback(response)
        },
    })
    request.get()

    return {
        abort: function () {
            aborted = true
        },
    }

}
