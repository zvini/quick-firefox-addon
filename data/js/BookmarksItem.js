function BookmarksItem (port,
    bookmark, selectListener, deselectListener) {

    function removeAll () {
        while (element.firstChild) element.removeChild(element.firstChild)
    }

    var url = bookmark.url
    var text = bookmark.title || url

    var classPrefix = 'BookmarksItem'

    var textNode = document.createTextNode(text)

    var element = document.createElement('div')
    element.className = classPrefix
    element.appendChild(textNode)
    element.addEventListener('mouseenter', selectListener)
    element.addEventListener('mousemove', selectListener)

    var linkClick = LinkClick(element, port, url)

    var classList = element.classList

    return {
        element: element,
        open: linkClick.open,
        openInNewTab: linkClick.openInNewTab,
        applyFilter: function (pattern) {

            removeAll()

            var regexp = new RegExp('(?:' + pattern + ')+', 'gi')
            var matches = text.match(regexp)
            if (matches === null) {
                classList.add('hidden')
                return false
            }

            var lastIndex = 0
            matches.forEach(function (match) {

                var index = text.indexOf(match, lastIndex)
                var leadingText = text.substr(index, match.length)
                var markText = text.substring(lastIndex, index)

                var mark = document.createElement('mark')
                mark.className = classPrefix + '-mark'
                mark.appendChild(document.createTextNode(leadingText))

                element.appendChild(document.createTextNode(markText))
                element.appendChild(mark)

                lastIndex = index + match.length

            })
            element.appendChild(document.createTextNode(text.substr(lastIndex)))

            classList.remove('hidden')
            return true

        },
        deselect: function () {
            classList.remove('selected')
            element.removeEventListener('mouseleave', deselectListener)
            element.addEventListener('mouseenter', selectListener)
            element.addEventListener('mousemove', selectListener)
        },
        reset: function () {
            removeAll()
            classList.remove('hidden')
            element.appendChild(textNode)
        },
        select: function () {
            classList.add('selected')
            element.removeEventListener('mouseenter', selectListener)
            element.removeEventListener('mousemove', selectListener)
            element.addEventListener('mouseleave', deselectListener)
        },
    }

}
