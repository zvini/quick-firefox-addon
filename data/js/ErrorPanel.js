function ErrorPanel (port) {

    var classPrefix = 'ErrorPanel'

    var info = 'An error occured while loading the data.'
    var infoNode = document.createTextNode(info)

    var infoElement = document.createElement('div')
    infoElement.className = classPrefix + '-info'
    infoElement.appendChild(infoNode)

    var buttonElement = document.createElement('button')
    buttonElement.className = classPrefix + '-button'
    buttonElement.appendChild(document.createTextNode('Retry'))
    buttonElement.addEventListener('click', function () {
        port.emit('retry')
    })

    var element = document.createElement('div')
    element.className = classPrefix
    element.appendChild(infoElement)
    element.appendChild(buttonElement)

    return { element: element }

}
